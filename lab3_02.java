import java.util.Scanner;
public class lab3_02 {
    public static int searchIndex(int[] arr,int target){
        int left = 0;
        int right = arr.length-1;       

        while (left <= right) {
            int mid = left + (right - left) / 2;

            if (arr[mid] == target) {
                return mid; // Return the index of the target element
            } else if (arr[mid] < target) {
                left = mid + 1; // Discard the left half of the array
            } else {
                right = mid - 1; // Discard the right half of the array
            }
        }

        return -1; // Return -1 if the target element is not found
    }

    public static void orderArray(int[] nums, int k) {
        int n = nums.length;
        k = k % n;
        
        reverse(nums, 0, n - 1);
        printArray(nums);
        reverse(nums, 0, k - 1);
        printArray(nums);
        reverse(nums, k, n - 1);
        printArray(nums);
    }
    
    public static void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }
            
    

    private static void printArray(int[] arr){
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] nums = {0,1,2,4,5,6,7};
        int pvtIndex = 2;
        int target;

        orderArray(nums,pvtIndex);
        

        target = sc.nextInt();   

        int index = searchIndex(nums, target);

        if (index != -1) {
            System.out.println("Element " + target + " found at index " + index);
        } else {
            System.out.println("Element " + target + " not found in the array");
        }
    }
}

